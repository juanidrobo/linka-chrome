
 

$(document).ready(function () {

  
    var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
    //console.log(linkaWidgetOptions);
    if (linkaWidgetOptions === null) {
        var options = new Object();
        options.documents = true;
        options.images = true;
        options.window = false;
        localStorage.setItem("linkaWidgetOptions", JSON.stringify(options));
    }


    main();


});

$(window).bind("beforeunload", function () {

    localStorage.setItem("linkaRunning", false);

});


function main()
{

    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        console.log(tabs);
        var lastFocusedurl = tabs[0].url;
        //alert("main:" + lastFocusedurl);
        $("#urlid").val(lastFocusedurl);
        console.log(lastFocusedurl);

        var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
        var options = JSON.parse(linkaWidgetOptions);

        if (options.window) {
            checkForAlreadyOpenedPopup(lastFocusedurl);
        } else {
            checkForWidget(lastFocusedurl);
        }


    });

}

function checkForWidget(lastFocusedurl) {

    var urlParameter = getUrlParameter('url');
    console.log("urlParameter : " + urlParameter);
    var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
    var options = JSON.parse(linkaWidgetOptions);
    var url = $("#urlid").val();

    if (options.window && (urlParameter === undefined)) {
        //added the linkacode=13 to identify the window
        //window.open("index.html?linkacode=13&url=" + url, "_blank", "width=330,height=500");
        if (localStorage.getItem("linkaRunning") != "true") {
            localStorage.setItem("linkaRunning", true);
            chrome.windows.create({"url": "index.html?linkacode=13&url=" + lastFocusedurl, "type": "popup", "width": 330, "height": 550}, function (window) {
                localStorage.setItem("linkaWindowId", window.id);

            });
        }
    }
    else {

        checkIfToCloseWindow();
    }

    startWidget();
  

}

function checkForAlreadyOpenedPopup(lastFocusedurl) {

    console.log("Check for AlreadyOpenedPopup");
    var exist = false;
    chrome.tabs.query({}, function (tabs) {

        for (var i = 0; i < tabs.length; i++) {

            //to check if a popup with this code exists
            if (tabs[i].url.includes("linkacode=13")) {
                activateAlreadyOpenedPopup(tabs[i].windowId, lastFocusedurl);
                exist = true;
                break;
            }
        }
        if (!exist) {
            checkForWidget(lastFocusedurl);
        }
    });

}


function activateAlreadyOpenedPopup(windowId, lastFocusedurl) {

    chrome.windows.onFocusChanged.addListener(changeUrl);

    try {
        chrome.windows.update(windowId, {"focused": true}, function () {
            //this gets executed just the first time, no fu%$%$# idea why, added the onFocusChanged to make it work
            $("#urlid").val(getUrlParameter("url"));
            checkForWidget();
        });
    } catch (err) {
        console.log("error: " + err);
    }

}

function changeUrl() {
    chrome.tabs.query({'active': true}, function (tabs) {
        var lastFocusedurl = tabs[0].url;
        $("#urlid").val(lastFocusedurl);
        reStartWidget();
    });
}


function resizeHeight()
{
    var urlParameter = getUrlParameter("url");
    if (urlParameter !== undefined) {
        var linkaWindowId = parseInt(localStorage.getItem("linkaWindowId"));
        var heightToResize = $("iframe").height() + 50;
        try {
            chrome.windows.update(linkaWindowId, {"height": heightToResize}, function () {

            });
        } catch (err) {
        }
    }

}



