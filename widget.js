
function startWidget() {
    (function () {
        if (typeof window.linkapediaWidgetLoaded === "undefined") {
            window.linkapediaWidgetLoaded = true
        }
         var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
         var options= new Object();
         options.images=true;
         options.documents=true;
        if (linkaWidgetOptions!=null){
              options=JSON.parse(linkaWidgetOptions);
           
         }

        if (window.linkapediaWidgetLoaded) {
            var scriptTag = document.createElement('script');
            scriptTag.src = 'embed.js';
            scriptTag.async = true;
            //var protocol = $('<a>').prop('href', $("#urlid").val()).prop('protocol');
            //var hostname = $('<a>').prop('href', $("#urlid").val()).prop('hostname');
            //var blogid=md5(protocol+"//"+hostname+"/");
            window.linkapediaWidgetOptions = {"url": $("#urlid").val(), "hostname": "test.linkapedia.com", "design": "skycrapper", "withimages": options.images, "withdocuments": options.documents, "withborder": "true", "bordercolor": "#cccccc", "titlecolor": "#000000", "backgroundcolor": "#f2f2f2", "titleitemscolor": "#000000", "hoveritemscolor": "#999999", "hovertitleitemscolor": "#000000"};
            document.body.appendChild(scriptTag);
            window.linkapediaWidgetLoaded = false;
        }
    }());
}

function reStartWidget(){
    $("#com-linkapedia-widget-topics").html("");
     window.linkapediaWidgetLoaded = true;
     startWidget();
}