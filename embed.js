(function () {

    String.prototype.contains = function (text) {
        return this.toLowerCase().indexOf(text.toLowerCase()) !== -1;
    };

    //blog id is not needed here, pending to remove
  //  var blogId = window.linkapediaWidgetBlogId;
    var hostname = window.linkapediaWidgetOptions.hostname;
    var url = "http://" + hostname + "/widgets/topicsByDocumentExtension/documents/topics?";
    var design = window.linkapediaWidgetOptions.design;
    if (design === "topicmap" || design === "floattopicmap") {
        //url = "http://" + hostname + "/widgets/topicmap/" + blogId + "?";
        url = "http://" + hostname + "/widgets/topicmap/";
    }

    function addParameter(key, value) {
        url += "&" + key + "=" + window.btoa(value);
    }

    var key;
    for (key in window.linkapediaWidgetOptions) {
        addParameter(key, window.linkapediaWidgetOptions[key]);
    }

    addParameter("urlreferer", window.location.href);

    var container = document.getElementById("com-linkapedia-widget-topics");
    var iframe = document.createElement('iframe');
    var position = window.linkapediaWidgetOptions.position;

    if (design === "float" || design === "floattopicmap") {
        var width = container.getAttribute("style");
        var body = document.getElementsByTagName("body")[0];
        container.parentNode.removeChild(container);
        container = document.createElement("div");
        container.setAttribute("id", "com-linkapedia-widget-topics");
        body.insertBefore(container, body.firstChild);

        if (position === "right") {
            container.setAttribute("style", width + ";position:fixed;right: 0%;margin: 50vh auto 0;transform: translateY(-50%);");
        } else {
            container.setAttribute("style", width + ";position:fixed;left: 0%;margin: 50vh auto 0;transform: translateY(-50%);");
        }
    }

    var height = '0px';

    switch (design) {
        case 'float' :
            height = "400px";
            break;
        case 'topicmap':
            height = "42px";
            break;
        case 'floattopicmap':
            height = "400px";
            break;
    }

    if (design === "topicmap" && window.linkapediaWidgetOptions.withexpandbutton === "false") {
        height = "376px";
    }

    container.style.height = height;

    if (design === "topicmap") {
        // container.style.display = "none";
    }

    container.style.zIndex = "99999999999";
    container.appendChild(iframe);

    iframe.style.width = "100%";
    iframe.style.height = "100%";
    iframe.src = url;
    iframe.id = 'com-linkapedia-widget-topics-iframe';
    iframe.frameBorder = 0;
    iframe.scrolling = 'no';

    var contentWindow = iframe.contentWindow;

    window.addEventListener('message', function (event) {
        if (event.origin.contains("localhost") || event.origin.contains("linkapedia.com")) {
            var action = event.data.split(",")[0];
            var value = event.data.split(",")[1];

            if (action === "update_div_float_design") {
                if (position === "right") {
                    if (container.style.marginRight === "-335px") {
                        openFloatRight();
                    } else {
                        closeFloatRight();
                    }
                } else {
                    if (container.style.marginLeft === "-335px") {
                        openFloatLeft();
                    } else {
                        closeFloatLeft();
                    }
                }

            } else if (action === "get_width_from_content_iframe") {
                container.style.width = value + "px";
            } else {
                container.style.height = value + "px";
            }

            if (action === "showcontentexpanded_topicmap") {
                container.style.height = "376px";
                contentWindow.postMessage("iframe_is_expanded_topicmap", "*");
            } else if (action === "hidecontentexpanded_topicmap") {
                container.style.height = "42px";
                contentWindow.postMessage("iframe_is_not_expanded_topicmap", "*");
            }

            if (action === "topicmap_firstloadready") {
                container.style.display = "block";
            }

            if (action === "update_height_div") {
                container.style.height = value;
            }
        } else {
            return;
        }
    }, false);

    var blocked = false;

    function closeFloatRight() {
        var marginRight = 0;
        if (blocked) {
            return;
        }
        blocked = true;

        function frame() {
            marginRight -= 2;

            if (marginRight <= -335) {
                container.style.marginRight = "-335px";
                clearInterval(id);
                blocked = false;
            } else {
                container.style.marginRight = marginRight + "px";
            }
        }

        var id = setInterval(frame, 1);
    }

    function openFloatRight() {
        var marginRight = -335;
        if (blocked) {
            return;
        }
        blocked = true;
        function frame() {
            marginRight += 2;

            if (marginRight >= 0) {
                container.style.marginRight = "0px";
                clearInterval(id);
                blocked = false;
            } else {
                container.style.marginRight = marginRight + "px";
            }
        }

        var id = setInterval(frame, 1);
    }

    function closeFloatLeft() {
        var marginLeft = 0;
        if (blocked) {
            return;
        }
        blocked = true;

        function frame() {
            marginLeft -= 2;

            if (marginLeft <= -335) {
                container.style.marginLeft = "-335px";
                clearInterval(id);
                blocked = false;
            } else {
                container.style.marginLeft = marginLeft + "px";
            }
        }

        var id = setInterval(frame, 1);
    }

    function openFloatLeft() {
        var marginLeft = -335;
        if (blocked) {
            return;
        }
        blocked = true;
        function frame() {
            marginLeft += 2;

            if (marginLeft >= 0) {
                container.style.marginLeft = "0px";
                clearInterval(id);
                blocked = false;
            } else {
                container.style.marginLeft = marginLeft + "px";
            }
        }

        var id = setInterval(frame, 1);
    }
}());