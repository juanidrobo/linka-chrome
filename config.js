
$(document).ready(function () {


    var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
    //console.log(linkaWidgetOptions);
    if (linkaWidgetOptions === null) {
        var options = new Object();
        options.documents = true;
        options.images = true;
        options.window = false;
        // localStorage.setItem("linkaWidgetOptions", JSON.stringify(options));
    } else
    {
        var options = JSON.parse(linkaWidgetOptions)
        if (options.images) {
            $("#input_images").attr("checked", true);
        } else {
            $("#input_images").attr("checked", false);
        }
        if (options.documents) {
            $("#input_documents").attr("checked", true);
        } else {
            $("#input_documents").attr("checked", false);
        }
        if (options.window) {
            $("#input_window").attr("checked", true);
        } else {
            $("#input_window").attr("checked", false);
        }
    }


    $("#a_config").click(function () {
        var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
        var options = JSON.parse(linkaWidgetOptions)
        localStorage.setItem("old_image_opt", options.images);
        localStorage.setItem("old_documents_opt", options.documents);
        $("#index").removeClass("visible");
        $("#index").addClass("hidden");
        $("#config").removeClass("hidden");
        $("#config").addClass("visible");


    });
    $("#a_back").click(function () {


        $("#index").removeClass("hidden");
        $("#index").addClass("visible");
        $("#config").removeClass("visible");
        $("#config").addClass("hidden");
        checkIfToCloseWindow();


        checkForChangesToOpt();

    });

    $('#input_images').click(function () {
        var $this = $(this);
        // $this will contain a reference to the checkbox  
        var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
        var options = JSON.parse(linkaWidgetOptions);

        if ($this.is(':checked')) {
            options.images = true;
        } else {
            options.images = false;
            // the checkbox was unchecked
        }
        localStorage.setItem("linkaWidgetOptions", JSON.stringify(options));
    });

    $('#input_documents').click(function () {
        var $this = $(this);
        // $this will contain a reference to the checkbox  
        var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
        var options = JSON.parse(linkaWidgetOptions);

        if ($this.is(':checked')) {
            options.documents = true;
        } else {
            options.documents = false;
            // the checkbox was unchecked
        }
        localStorage.setItem("linkaWidgetOptions", JSON.stringify(options));
    });
    $('#input_window').click(function () {
        var $this = $(this);
        // $this will contain a reference to the checkbox  
        var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
        var options = JSON.parse(linkaWidgetOptions);

        if ($this.is(':checked')) {
            options.window = true;

        } else {
            options.window = false;
        }
        localStorage.setItem("linkaWidgetOptions", JSON.stringify(options));
    });


});


function checkForChangesToOpt() {
    var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
    var options = JSON.parse(linkaWidgetOptions)
    if (options.images.toString() !== localStorage.getItem("old_image_opt"))
    {
        location.reload();
    }
    if (options.documents.toString() !== localStorage.getItem("old_documents_opt"))
    {
        location.reload();
    }

}

function checkIfToCloseWindow() {

    var linkaWidgetOptions = localStorage.getItem("linkaWidgetOptions");
    var urlParameter = getUrlParameter("url");
    var options = JSON.parse(linkaWidgetOptions);

    if (!options.window && (urlParameter !== undefined)) {
        window.close();
    }
    if (options.window && (urlParameter === undefined)) {
        location.reload();
    }
}





